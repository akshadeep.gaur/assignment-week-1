package com.greatLearning.assignment;

public class HrDepartment extends SuperDepartment {
	
	public String deparmentName(){
		return "HR Department";		
	}
	
	public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendance";
	}
	
	public String doActivity() {
		return "Team Lunch";
	}
	
	public String getWorkDeadline() {
		return "Complete by EOD";
	}

}
